// Serve © 2015–2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

/// A **request script** encompasses the data of a request.
public struct RequestScript {
	
	/// The action.
	public var action: Action
	public enum Action : String {
		case GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, CONNECT, PATCH, PROPFIND, PROPPATCH, MKCOL, COPY, MOVE, LOCK, UNLOCK
		public static let all: Set<Action> = [.GET, .HEAD, .POST, .PUT, .DELETE, .TRACE, .OPTIONS, .CONNECT, .PATCH, .PROPFIND, PROPPATCH, .MKCOL, .COPY, .MOVE, .LOCK, .UNLOCK]
	}
	
	/// The target.
	public var target: Target
	public struct Target {
		
		/// The URL-decoded segments of the target.
		var segments: [String]
		
		/// The URL-decoded query.
		var query: String
		
	}
	
	/// The inner script.
	public var script: Script
	
}