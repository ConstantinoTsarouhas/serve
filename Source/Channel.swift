// Serve © 2015–2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

import Foundation

/// A **channel** makes an HTTP server available for clients by listening for new connections on a socket. Once a connection has been created, it's passed on to a channel responder who becomes responsible for managing it—the channel resigns its ownership over it at that stage.
public final class Channel {
	
	/// Creates and starts a channel.
	///
	/// - Parameter port: The port on which to listen for connections. The default is port 80.
	/// - Parameter internetProtocol: The Internet Protocol on which to bind the session to. The default is IPv6.
	/// - Parameter dispatchQueue: The dispatch queue for the channel, which needn't be serial (that guarantee is provided by the channel itself). The default is the global concurrent queue of the default QoS class.
	public init(port: Int = 80, internetProtocol: InternetProtocol = .ip6, dispatchQueue outerQueue: dispatch_queue_t = dispatch_get_global_queue(QOS_CLASS_DEFAULT, 0)) throws {
		
		self.port = port
		self.internetProtocol = internetProtocol
		
		dispatchQueue = dispatch_queue_create("constantinotsarouhas.Serve.ChannelQueue", dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_SERIAL, QOS_CLASS_BACKGROUND, 0))
		dispatch_set_target_queue(dispatchQueue, outerQueue)
		
		do {
			let qos = dispatch_queue_get_qos_class(outerQueue, nil)
			let globalQueue = dispatch_get_global_queue((qos == QOS_CLASS_UNSPECIFIED ? QOS_CLASS_DEFAULT : qos), 0)
			generateDispatchQueueForConnection = { globalQueue }
		}
		
		guard port >= 0 && port < (2 << 16) else {
			socket = Socket(identifier: 0)
			dispatchSource = nil
			throw InitialisationError.invalidPortNumber(port: port)
		}
		
		guard let socket = Socket(internetProtocol: internetProtocol) else {
			dispatchSource = nil
			throw InitialisationError.creation(code: Int(errno))
		}
		self.socket = socket
		
		do {
			var yes = 1
			guard setsockopt(socket.identifier, SOL_SOCKET, SO_REUSEADDR, &yes, socklen_t(sizeofValue(yes))) == 0 else {
				dispatchSource = nil
				throw InitialisationError.configuration(code: Int(errno))
			}
		}
		
		do {
			let (address, addressLength) = internetProtocol.makeAddress(port: port)
			guard bind(socket.identifier, address, addressLength) == 0 else {
				dispatchSource = nil
				throw InitialisationError.binding(code: Int(errno))
			}
		}
		
		guard listen(socket.identifier, 0) == 0 else {
			dispatchSource = nil
			throw InitialisationError.listening(code: Int(errno))
		}
		
		dispatchSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_READ, UInt(socket.identifier), 0, dispatchQueue)
		dispatch_source_set_event_handler(dispatchSource) { [weak self] in
			
			guard let channel = self else { return }
			
			guard let socket = Socket(listeningSocket: channel.socket) else {
				if errno == EAGAIN || errno == EWOULDBLOCK {
					return channel.responder?.handle(connectionBindingError: SocketAcceptanceError.unavailable, channel: channel) ?? ()
				} else {
					return channel.responder?.handle(connectionBindingError: SocketAcceptanceError.other(code: Int(errno)), channel: channel) ?? ()
				}
			}
			
			do {
				let connection = try Connection(socket: socket, dispatchQueue: channel.generateDispatchQueueForConnection()) 	// The connection is closed at end of scope if no responder is set.
				channel.responder?.accept(connection: connection, channel: channel)
			} catch {
				channel.responder?.handle(connectionBindingError: error, channel: channel)
			}
			
		}
		
	}
	
	/// An **initialisation error** can be thrown at initialisation time of a listening socket.
	public enum InitialisationError : ErrorProtocol {
		
		/// The port number is invalid.
		///
		/// - Parameter port: The port number.
		case invalidPortNumber(port: Int)
		
		/// The creation of the socket couldn't be done.
		///
		/// - Parameter code: The internal socket error code.
		case creation(code: Int)
		
		/// The configuration of the socket couldn't be done.
		///
		/// - Parameter code: The internal socket error code.
		case configuration(code: Int)
		
		/// The binding of the socket couldn't be done.
		///
		/// - Parameter code: The internal socket error code.
		case binding(code: Int)
		
		/// The listening to the socket couldn't be done.
		///
		/// - Parameter code: The internal socket error code.
		case listening(code: Int)
		
	}
	
	/// A **socket acceptance error** can be thrown while a socket is being prepared to be passed into a new connection.
	public enum SocketAcceptanceError : ErrorProtocol {
		
		/// The socket couldn't be accepted because it wasn't available by the time the event handler was called.
		case unavailable
		
		/// The socket couldn't be accepted due to another error.
		///
		/// - Parameter code: The internal socket error code.
		case other(code: Int)
		
	}
	
	/// The port number.
	public let port: Int
	
	/// The underlying Internet Protocol.
	public let internetProtocol: InternetProtocol
	public enum InternetProtocol {
		
		case ip4, ip6
		
		/// Generates an address structure for inbound connections with given port number applicable to the Internet Protocol.
		///
		/// - Parameter port: The port number.
		@warn_unused_result private func makeAddress(port: Int) -> (address: UnsafePointer<sockaddr>, length: socklen_t) {
			
			func cast<I, O>(pointer: UnsafePointer<I>) -> UnsafePointer<O> {
				return UnsafePointer<O>(pointer)
			}
			
			let family = sa_family_t(addressFamily)
			let port = in_port_t(port).bigEndian
			
			switch self {
				
				case .ip4:
				var address = sockaddr_in()
				let length = sizeofValue(address)
				address.sin_len = UInt8(length)
				address.sin_family = family
				address.sin_port = port
				address.sin_addr = in_addr(s_addr: 0)
				return (cast(pointer: &address), socklen_t(socklen_t(length)))
				
				case .ip6:
				var address = sockaddr_in6()
				let length = sizeofValue(address)
				address.sin6_len = UInt8(length)
				address.sin6_family = family
				address.sin6_port = port
				address.sin6_addr = in6addr_any
				return (cast(pointer: &address), socklen_t(length))
				
			}
			
		}
		
		private var addressFamily: Int32 {
			switch self {
				case ip4: return AF_INET
				case ip6: return AF_INET6
			}
		}
		
		private var protocolFamily: Int32 {
			switch self {
				case ip4: return PF_INET
				case ip6: return PF_INET6
			}
		}
		
	}
	
	/// The socket.
	private let socket: Socket
	
	/// The dispatch queue on which the channel operates, which serialises all accesses to the channel. It is guaranteed to be serial.
	private let dispatchQueue: dispatch_queue_t
	
	/// The dispatch source.
	///
	/// - Note: The source is always set in fully initialised channels.
	private let dispatchSource: dispatch_source_t!
	
	/// The channel's responder, which accepts new connections and handles connection errors (on the channel's dispatch queue). Connections are immediately closed whilst this property is unset.
	///
	/// - Warning: Every access to this property must occur within the context of the channel's dispatch queue.
	public weak var responder: ChannelResponder?
	
	/// The generator of dispatch queues for new connections. The queues needn't be serial. The default generator returns the global queue of the same QoS as the channel's dispatch queue.
	///
	/// - Warning: Every access to this property must occur within the context of the channel's dispatch queue.
	public var generateDispatchQueueForConnection: () -> dispatch_queue_t
	
	/// Cancels the listening session's dispatch source and closes the listening session's socket.
	deinit {
		
		if let source = dispatchSource {
			dispatch_source_cancel(source)
		}
		
		if socket.identifier > 0 {
			close(socket.identifier)
		}
		
	}
	
}

extension Channel : Hashable {	// TODO: Any need for this?
	public var hashValue: Int {
		return socket.identifier.hashValue
	}
}

public func ==(l: Channel, r: Channel) -> Bool {	// TODO: Any need for this?
	return l.socket.identifier == r.socket.identifier
}

internal struct Socket {
	
	private init?(internetProtocol: Channel.InternetProtocol) {
		
		let identifier = socket(internetProtocol.protocolFamily, SOCK_STREAM, IPPROTO_TCP)
		guard identifier > 0 else {
			return nil
		}
		
		self.identifier = identifier
		
	}
	
	private init?(listeningSocket: Socket) {
		
		var address = sockaddr()	// TODO: Store somewhere?
		var addressLength = socklen_t(sizeofValue(address))
		
		let identifier = accept(listeningSocket.identifier, &address, &addressLength)
		guard identifier > 0 else {
			return nil
		}
		
		self.identifier = identifier
		
	}
	
	private init(identifier: Int32) {
		precondition(identifier >= 0, "The identifier must be nonnegative.")
		self.identifier = identifier
	}
	
	internal let identifier: Int32
	
}