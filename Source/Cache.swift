// Serve © 2015–2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

/// A cache object generates and caches a value upon first request and regenerates and recaches it upon first request after invalidation.
///
/// - Note: `Cache` is implemented as a reference type so that structures don't have to mutate when fetching. To preserve the immutability properties of constant structures,
///		- use `Cache` for caching purposes only;
///		- provide a generator whose output doesn't change when the structure doesn't change; and
///		- don't expose the cache in public API, and if possible, neither in internal API.
public final class Cache<T> {
	
	/// Initialises a cache with given generator.
	public init(generator: () -> T) {
		self.generator = generator
	}
	
	/// The cached value, generating it first if needed.
	public var value: T {
		if let v = cachedValue {
			return v
		} else {
			let v = generator()
			cachedValue = v
			return v
		}
	}
	
	/// Invalidates the cache.
	public func invalidate() {
		cachedValue = nil
	}
	
	/// The generator that generates the cached value whenever needed.
	private let generator: () -> T
	
	/// The cached value, if available.
	private var cachedValue: T?
	
}