// Serve © 2015–2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

/// A **connection responder** accepts new requests from a connection.
public protocol ConnectionResponder : class {
	
	/// Accepts given request.
	///
	/// - Parameter 1: The request to accept.
	func accept(request: Request)
	
}