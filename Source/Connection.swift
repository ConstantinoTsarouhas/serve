// Serve © 2015–2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

import Foundation

/// A **connection** receives and sends bytes (more technically, *octets*) through a socket it owns and manages.
///
/// The connection object generates requests from received data and vends responses by sending data. HTTP/1.0 connections typically only last for one request–response cycle while HTTP/1.1 connections can have multiple such cycles.
///
/// The connection remains the owner of the requests it generates, as well as the responses assigned to those requests. Request and response objects become invalid as soon as the connection object is discarded.
///
/// A connection object that isn't owned by any external object remains alive by itself as long as the underlying TCP connection remains open.
public final class Connection {
	
	/// Initialises a connection object with given socket identifier.
	///
	/// - Parameter socket: The socket to the HTTP client. The socket is owned by the connection after initialisation and thus may no longer be used outside of the connection.
	/// - Parameter dispatchQueue: The dispatch queue for the connection, which needn't be serial (that guarantee is provided by the connection itself). The default is the global concurrent queue of the default QoS class.
	internal init(socket: Socket, dispatchQueue outerQueue: dispatch_queue_t = dispatch_get_global_queue(QOS_CLASS_DEFAULT, 0)) throws {
		
		dispatchQueue = dispatch_queue_create("constantinotsarouhas.Serve.ConnectionQueue", dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_SERIAL, QOS_CLASS_BACKGROUND, 0))
		dispatch_set_target_queue(dispatchQueue, outerQueue)
		
		do {
			var yes = 1
			guard setsockopt(socket.identifier, SOL_SOCKET, SO_NOSIGPIPE, &yes, socklen_t(sizeofValue(yes))) == 0 else { throw InitialisationError.pipeSignalRepressionError(code: Int(errno)) }
		}
		
		try stream.openStream(socket: socket, dispatchQueue: dispatchQueue)
		
	}
	
	public enum InitialisationError : ErrorProtocol {
		
		/// An error has occurred while opening the stream.
		case streamOpeningError
		
		/// An error has occurred while configuring the socket to repress pipe signals.
		case pipeSignalRepressionError(code: Int)
		
		/// An error has occurred while reading from the stream.
		case streamReadingError(code: Int)
		
	}
	
	/// The dispatch queue on which the channel operates, which serialises all accesses to the channel. It is guaranteed to be serial.
	private let dispatchQueue: dispatch_queue_t
	
	/// Whether the connection is open.
	///
	/// - Warning: Every access to this property must occur within the context of the connection's dispatch queue.
	public var open: Bool {
		if case .open = stream {
			return true
		} else {
			return false
		}
	}
	
	/// The underlying stream.
	///
	/// - Warning: Every access to this property must occur within the context of the connection's dispatch queue.
	private var stream = Stream.unopened {
		willSet { assert(stream.isValidTransition(to: newValue), "The transition of stream state is invalid.") }
	}
	private enum Stream {
		
		/// The stream has never been opened.
		case unopened
		
		/// The stream is open.
		///
		/// - Parameter 1: The underlying stream.
		case open(dispatch_io_t)
		
		/// The stream is closed.
		case closed
		
		/// Whether the transition to given state is valid.
		///
		/// - Parameter to: The state to transition to.
		///
		/// - Returns: Whether the transition to given state is valid.
		func isValidTransition(to: Stream) -> Bool {
			
			func ordinal(of: Stream) -> Int {
				switch (of) {
					case .unopened:		return 0
					case .open:			return 1
					case .closed:		return 2
				}
			}
			
			return ordinal(of: self) <= ordinal(of: to)
			
		}
		
		/// Opens the stream.
		///
		/// - Parameter socket: The socket on which to open the stream.
		/// - Parameter dispatchQueue: The dispatch queue on which stream events are processed.
		///
		/// - Throws: `InitialisationError.streamCreationError` if the stream couldn't be opened.
		///
		/// - Requires: The stream is unopened.
		mutating func openStream(socket: Socket, dispatchQueue: dispatch_queue_t) throws {
			
			guard case .unopened = self else { fatalError("The stream must be unopened.") }
			guard let stream = dispatch_io_create(DISPATCH_IO_STREAM, socket.identifier, dispatchQueue, { Foundation.close($0) }) else { throw InitialisationError.streamOpeningError }
			self = .open(stream)
			
			dispatch_io_read(stream, 0, Connection.maximumBufferSize, dispatchQueue) { /*[weak self]*/ done, data, error in
				// TODO: Parse message.
			}
			
		}
		
		/// Closes the stream if it is open.
		mutating func closeStream() {
			guard case .open(let stream) = self else { return }
			dispatch_io_close(stream, 0)
			self = .closed
		}
		
	}
	
	/// The current request message.
	///
	/// - Warning: Every access to this property must occur within the context of the connection's dispatch queue.
	private var currentRequestMessage = 0	// TODO
	
	/// The default maximum buffer size of just over a megabyte (more precisely, a mebibyte or 2<sup>20</sup> bytes).
	public static let maximumBufferSize = 2 << 20
	
	/// The requests that haven't been requited or whose responses haven't been vended yet, ordered from oldest to newest request.
	///
	/// The last request in the array is guaranteed to be the same as the request that is passed to the connection responder (if any is set) by the time the connection notifies the responder. This implies that the array is guaranteed to be nonempty whilst (and only whilst) the connection responder is being notified.
	///
	/// - Warning: Every access to this property must occur within the context of the connection's dispatch queue.
	public private(set) var unvendedRequests = [Request]()
	
	/// Registers given response for vending.
	///
	/// - Parameter response: The response to vend.
	///
	/// - Warning: Every invocation of this method must occur within the context of the connection's dispatch queue.
	internal func register(response: Response) {
		
		guard unvendedRequests.first === response.request else { return }
		
		// TODO: Schedule vending.
		
	}
	
	/// The connection's responder, which accepts new requests.
	///
	/// The responder (if one is set) is notified on the connection's dispatch queue immediately after the request has been added to `unvendedRequests`. The channel guarantees serialisation: no access to `unvendedRequests` is permitted between the request being added to the array and the responder being notified. When the responder returns, other subsystems may access `unvendedRequests` and perform processing.
	///
	/// - Warning: Every access to this property must occur within the context of the connection's dispatch queue.
	public weak var responder: ConnectionResponder?
	
	deinit {
		stream.closeStream()
	}
	
}