// Serve © 2015–2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

/// A **script** encompasses the data of a request or response.
public struct Script {
	
	/// The arguments.
	public var arguments: [(String, String)]
	
	/// The message.
	public var message: BinarySequence
	
}