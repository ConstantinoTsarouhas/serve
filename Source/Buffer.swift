// Serve © 2015–2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

/// A **buffer** dispatches messages to a responder, coalescing them if no responder is known.
///
/// - Warning: All accesses to the buffer must be serialised.
public final class Buffer<Responder : AnyObject> {
	
	/// The responder.
	public var responder : Responder? {
		
		get {
			switch state {
				case .coalescing(messages: _):			return nil
				case .forwarding(responder: let r):		return r.responder
			}
		}
		
		set {
			if let responder = newValue {
				switch state {
					
					case .coalescing(messages: let m):
					for message in m {
						message(responder)
					}
					fallthrough
					
					default:
					state = .forwarding(responder: Box(responder: responder))
					
				}
			}
		}
		
	}
	
	/// The buffer's state.
	///
	/// - Warning: This property does not dispatch coalesced messages. Dispatch coalesced messages before changing this property.
	private var state = State<Responder>.coalescing(messages: [])
	
	/// Dispatches a message.
	///
	/// - Parameter message: The message to dispatch.
	private func dispatchMessage(message: Message) {
		switch state {
			
			case .coalescing(messages: let m):
			state = .coalescing(messages: m + [message])
			
			case .forwarding(responder: let r):
			if let responder = r.responder {
				message(responder)
			} else {
				state = .coalescing(messages: [message])
			}
			
		}
	}
	
	/// A message is a method call on an object.
	public typealias Message = (Responder) -> ()
	
}

private enum State<Responder : AnyObject> {
	
	/// The buffer is coalescing.
	///
	/// - Parameter messages: The coalesced messages, from oldest to newest.
	case coalescing(messages: [Buffer<Responder>.Message])
	
	/// The buffer is forwarding.
	///
	/// - Parameter responder: The responder.
	case forwarding(responder: Box<Responder>)
	
}

private struct Box<Responder : AnyObject> {
	weak var responder: Responder?
}