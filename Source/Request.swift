// Serve © 2015–2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

import X

/// A **request** represents a live HTTP request that has been received by a connection.
///
/// A request is initially unrequited, i.e., no response has been assigned to it. When a response is assigned, the request becomes requited and the connection schedules its vending.
///
/// The connection is the sole owner of a request object. A request object must be discarded after its response has been vended. The request's script can be persisted if need be.
public final class Request {
	
	/// Initialises a request.
	internal init(connection: Connection, script: RequestScript) {
		self.connection = connection
		self.script = script
	}
	
	/// The connection that received the request.
	///
	/// The request object may not be persisted beyond its connection's lifespan. Preferably, it should be discarded after vending. If the need for it arises, the request's and the response's *scripts* can be persisted instead.
	public unowned let connection: Connection
	
	/// The script.
	public let script: RequestScript
	
	/// The response.
	public internal(set) var response: Response? = nil {
		willSet { assert(nilToSome.isValidTransition(from: response, to: newValue, differentObjectAllowed: false), "The request can't be reassigned to another response.") }
	}
	
}