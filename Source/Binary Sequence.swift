// Serve © 2015–2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

import Foundation

public struct BinarySequence {
	
	/// Initialises the binary sequence with given `dispatch_data_t` buffer.
	@warn_unused_result
	public init(_ buffer: dispatch_data_t = dispatch_data_empty) {
		self.buffer = buffer
	}
	
	/// Initialises the binary sequence with given `NSData` buffer.
	@warn_unused_result
	public init(_ buffer: NSData) {
		self.buffer = buffer.length > 0 ? dispatch_data_create(buffer.bytes, buffer.length, nil, nil) : dispatch_data_empty
	}
	
	/// The buffer.
	public var buffer: dispatch_data_t
	
	/// The binary sequence's representation in the Foundation framework, using `NSData`.
	public var foundationRepresentation: NSData {
		return unsafeBitCast(buffer, to: NSData.self)
	}
	
}

extension BinarySequence : Collection {
	
	public var startIndex: Int {
		return 0
	}
	
	public var endIndex: Int {
		return count
	}
	
	public func index(after: Int) -> Int {
		return after + 1
	}
	
	public subscript(index: Int) -> Byte {
		
		var result: Byte = 0
		
		dispatch_data_apply(self[index...index].buffer) { (_, _, pointer, _) in
			let a = UnsafePointer<Byte>(pointer!).pointee
			result = a
			return false
		}
		
		return result
		
	}
	
	public subscript(range: Range<Int>) -> BinarySequence {
		precondition(range.lowerBound >= startIndex)
		precondition(range.upperBound <= endIndex)
		return BinarySequence(dispatch_data_create_subrange(buffer, range.lowerBound, range.count))
	}
	
	public var count: Int {
		return dispatch_data_get_size(buffer)
	}
	
	@warn_unused_result
	public func makeIterator() -> BinarySequenceGenerator {
		return BinarySequenceGenerator(buffer: buffer)
	}
	
	public struct BinarySequenceGenerator : IteratorProtocol {
		
		/// Initialises a generator.
		@warn_unused_result
		public init(buffer: dispatch_data_t) {
			
			self.buffer = buffer
			
			var regions = [Region]()
			dispatch_data_apply(buffer) { (buffer, _, pointer, size) in
				regions.append(Region(buffer: buffer!, pointer: UnsafeBufferPointer(start: UnsafePointer<Byte>(pointer), count: size)))
				return true
			}
			generatorOfRegions = regions.makeIterator()
			
		}
		
		/// The underlying buffer.
		private let buffer: dispatch_data_t
		
		/// The generator of regions.
		private var generatorOfRegions: IndexingIterator<[Region]>
		private typealias Region = (buffer: dispatch_data_t, pointer: UnsafeBufferPointer<Byte>)
		
		/// The generator of the current buffer.
		private var generatorOfCurrentBuffer: UnsafeBufferPointerIterator<Byte>?
		
		/// See protocol.
		public mutating func next() -> Byte? {
			if let nextByte = generatorOfCurrentBuffer?.next() {
				return nextByte
			} else {
				if let generatorOfNextBuffer = generatorOfRegions.next()?.pointer.makeIterator() {
					generatorOfCurrentBuffer = generatorOfNextBuffer
					return next()
				} else {
					return nil
				}
			}
		}
		
	}
	
}

public typealias Byte = UInt8

@warn_unused_result
public func +(l: BinarySequence, r: BinarySequence) -> BinarySequence {
	return BinarySequence(dispatch_data_create_concat(l.buffer, r.buffer))
}

public func +=(l: inout BinarySequence, r: BinarySequence) {
	l.buffer = dispatch_data_create_concat(l.buffer, r.buffer)
}