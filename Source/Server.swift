// Serve © 2015–2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

import Foundation

/// A server manages and coordinates listening sessions and connections and delegates request responses to request responders.
public final class Server {
	
	/// Initialises a server with given transfer version and port.
	/// The initialiser attempts to create both IPv4 and IPv6 listening sessions, falling gracefully to one if just one succeeds to establish.
	public convenience init(port: Int = 80, inboundConnectionErrorHandler errorHandler: (ErrorProtocol) -> () = { _ in () }, dispatchQueue: dispatch_queue_t = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) throws {
		
		do {
			let listeningSession6 = try Channel(port: port, internetProtocol: .ip6, dispatchQueue: dispatchQueue)
			do {
				let listeningSession4 = try Channel(port: port, internetProtocol: .ip4, dispatchQueue: dispatchQueue)
				self.init(listeningSessionConfiguration: .hybrid(legacy: listeningSession4, modern: listeningSession6), inboundConnectionErrorHandler: errorHandler)
			} catch {
				self.init(listeningSessionConfiguration: .modernOnly(listeningSession6), inboundConnectionErrorHandler: errorHandler)
			}
		} catch {
			let listeningSession4 = try Channel(port: port, internetProtocol: .ip4, dispatchQueue: dispatchQueue)
			self.init(listeningSessionConfiguration: .legacyOnly(listeningSession4), inboundConnectionErrorHandler: errorHandler)
		}
		
	}
	
	/// Initialises a server with given transfer version and listening socket configuration.
	///
	/// - Requires: The listening session configuration is valid and legal.
	public init(listeningSessionConfiguration: ChannelConfiguration, inboundConnectionErrorHandler errorHandler: (ErrorProtocol) -> () = { _ in () }) {
		
		precondition(listeningSessionConfiguration.isValid, "The listening socket configuration must be valid.")
		precondition(listeningSessionConfiguration.isLegal, "The listening socket configuration must be legal.")
		
		self.listeningSessionConfiguration = listeningSessionConfiguration
		self.inboundConnectionErrorHandler = errorHandler
		
		switch listeningSessionConfiguration {
			
			case .legacyOnly(let s):
			s.responder = self
			
			case .modernOnly(let s):
			s.responder = self
			
			case .hybrid(legacy: let l, modern: let m):
			l.responder = self
			m.responder = self
			
			default: fatalError()
			
		}
		
	}
	
	/// The server's listening session configuration.
	public let listeningSessionConfiguration: ChannelConfiguration
	
	public enum ChannelConfiguration : Equatable {
		
		/// The configuration is invalid.
		case invalid
		
		/// The server is configured with only a channel for legacy (IPv4) connections.
		case legacyOnly(Channel)
		
		/// The server is configured with only a channel for modern (IPv6) connections.
		case modernOnly(Channel)
		
		/// The server is configured with channels for both legacy (IPv4) and modern (IPv6) connections.
		case hybrid(legacy: Channel, modern: Channel)
		
		/// Whether the configuration is valid, i.e., whether `self != .invalid`.
		var isValid: Bool {
			return self != .invalid
		}
		
		/// Whether the configuration is legal, i.e., whether its channels have the correct Internet Protocol.
		var isLegal: Bool {
			switch self {
				case .invalid:					return true
				case .legacyOnly(let s):		return s.internetProtocol == .ip4
				case .modernOnly(let s):		return s.internetProtocol == .ip6
				case .hybrid(let l, let m):		return l.internetProtocol == .ip4 && m.internetProtocol == .ip6
			}
		}
		
	}
	
	/// The active connections.
	public private(set) var activeConnections = [Connection]()
	
	/// The error handler for inbound connections.
	public var inboundConnectionErrorHandler: (ErrorProtocol) -> ()
	
	public enum InboundConnectionError : ErrorProtocol {
		case noRespondingSocket
	}
	
}

extension Server : ChannelResponder {
	
	public func accept(connection: Connection, channel: Channel) {
		activeConnections.append(connection)
		// TODO
	}
	
	public func handle(connectionBindingError: ErrorProtocol, channel: Channel) {
		inboundConnectionErrorHandler(connectionBindingError)
	}
	
}

extension Server.ChannelConfiguration : Sequence {
	public func makeIterator() -> IndexingIterator<[Channel]> {
		switch self {
			case .invalid:					return [].makeIterator()
			case .legacyOnly(let s):		return [s].makeIterator()
			case .modernOnly(let s):		return [s].makeIterator()
			case .hybrid(let l, let m):		return [l, m].makeIterator()
		}
	}
}

public func ==(l: Server.ChannelConfiguration, r: Server.ChannelConfiguration) -> Bool {
	switch (l, r) {
		case (.invalid, .invalid):									return true
		case (.legacyOnly(let a), .legacyOnly(let b)):				return a == b
		case (.modernOnly(let a), .modernOnly(let b)):				return a == b
		case (.hybrid(let a1, let a2), .hybrid(let b1, let b2)):	return a1 == b1 && a2 == b2
		default:													return false
	}
}