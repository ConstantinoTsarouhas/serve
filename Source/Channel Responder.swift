// Serve © 2015–2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

/// A **channel responder** accepts new connections and handles any binding errors.
public protocol ChannelResponder : class {
	
	/// Accepts given connection.
	///
	/// - Parameter connection: The connection to accept.
	/// - Parameter channel: The channel.
	func accept(connection: Connection, channel: Channel)
	
	/// Handles an error that occurred during the binding of a new connection.
	///
	/// - Parameter connectionBindingError: The error.
	/// - Parameter channel: The channel.
	func handle(connectionBindingError: ErrorProtocol, channel: Channel)
	
}