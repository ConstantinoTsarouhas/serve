// Serve © 2015–2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

import X

/// A **response** represents a live HTTP response that has been associated with a request.
///
/// The request is the sole owner of a response object. A response object must be discarded after it has been vended. The response's script can be persisted if need be.
public final class Response {
	
	/// Initialises a response to a request.
	///
	/// - Parameter request: The request that is being requited by the response.
	/// - Parameter script: The script.
	/// - Parameter vendable: Whether the response can be vended at the connection's discretion.
	///
	/// - Requires: The request doesn't have already a response.
	private init(request: Request, script: ResponseScript, vendable: Bool = true) {
		precondition(request.response == nil)
		self.request = request
		self.script = script
		self.vendable = vendable
		request.response = self
		connection.register(response: self)
	}
	
	/// The request that is being requited by the response.
	///
	/// The response object may not be persisted beyond its request's lifespan. If the need for it arises, the *script* can be persisted instead.
	public unowned let request: Request
	
	/// The script.
	public let script: ResponseScript	// TODO: Make mutable (with constraints).
	
	/// The connection that is responsible for vending the response.
	public var connection: Connection {
		return request.connection
	}
	
	/// Whether the response can be vended at the connection's discretion.
	public var vendable: Bool {
		willSet {
			precondition(!vended || newValue, "The response cannot be rendered unvendable after it is vended.")
		}
	}
	
	/// Whether the response has been vended by the connection.
	public internal(set) var vended = false {
		willSet {
			assert(falseToTrue.isValidTransition(from: vended, to: newValue), "The response can't be un-vended once it is vended.")
		}
	}
	
}