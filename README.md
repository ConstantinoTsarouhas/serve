# The *Accipiter Commons* Serve framework
Serve is a Swift-based HTTP server API. Serve aims to be a small and concise framework for Swift-based server applications, without specialising in any HTTP application domain such as web applications but instead delegating these responsibilities to frameworks based on top of Serve.

## Feature set
* **HTTP/1.0** and **HTTP/1.1**
* Out-of-the-box HTTP **pipelining**
* Low-level *and* high-level APIs for both flexibility and ease-of-use
	* **Connection-oriented** request handling (low-level API)
	* **Responder-based** request handling API (cf. *Chain-of-responsibility* design pattern)
	* **Path-based** request handling API (also known as *routing*)
	* **Resource-oriented** request handling (cf. *RESTful*)
	* Any combination of the above
* **Platform-agnostic**
* Efficiently **concurrent** operation via *libdispatch*

The following features are scheduled for implementation after Serve reaches its first production version.

* HTTP over **TLS 1.2** (*HTTP Secure*)
* HTTP **basic authentication** (HTTPS only)
* **Chunked encoding**
* **HTTP/2.0**

## Using Serve
(…)