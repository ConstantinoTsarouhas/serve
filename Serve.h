// Serve © 2015–2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

@import Foundation;

FOUNDATION_EXPORT double ServeVersionNumber;
FOUNDATION_EXPORT const unsigned char ServeVersionString[];
